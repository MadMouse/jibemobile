package uk.co.madmouse.jibe.demo.friendfinder;

import jibe.sdk.client.apptoapp.Config;
import android.app.Application;

public class JibeApplication extends Application {

	public static final String APP_ID = "4a11267c2d5d4e159c4234fdd3186e72";
	public static final String APP_SECRET = "b29785e838e14e5dae1da43f22bbfa50";

	
	public void onCreate() {
		super.onCreate();
		// set up App-ID and App-Secret in one central place.
		Config.getInstance().setAppToAppIdentifier(APP_ID, APP_SECRET);

	}
}
