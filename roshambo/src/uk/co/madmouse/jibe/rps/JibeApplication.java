package uk.co.madmouse.jibe.rps;

import jibe.sdk.client.apptoapp.Config;
import android.app.Application;

public class JibeApplication extends Application {

	public static final String APP_ID = "e2544bc2772f4d3b9884f620341e2350";
	public static final String APP_SECRET = "c7e29fc7e2ed4f04a9febafc495ad229";

	public void onCreate() {
		super.onCreate();
		// set up App-ID and App-Secret in one central place.
		Config.getInstance().setAppToAppIdentifier(APP_ID, APP_SECRET);

	}
}
