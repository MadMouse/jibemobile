package uk.co.madmouse.jibe.rps;

import uk.co.madmouse.jibe.rps.utils.RPSHelper;
import jibe.sdk.client.JibeServiceException;
import jibe.sdk.client.JibeServiceListener.ConnectFailedReason;
import jibe.sdk.client.simple.SimpleApi;
import jibe.sdk.client.simple.SimpleApiStateListener;
import jibe.sdk.client.simple.arena.ArenaHelper;
import jibe.sdk.client.simple.myprofile.MyProfileHelper;
import jibe.sdk.client.simple.myprofile.MyProfileHelper.OnlineStateListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Bootstrap extends Activity {
	private static final String TAG = Bootstrap.class.getSimpleName();

	private MyProfileHelper mMyProfileHelper;
	private ArenaHelper mArenaHelper;
	private AlertDialog mDownloadDialog;
	private boolean mIsEngineInstalled;

	private TextView mStatusView = null;
	private ImageView mImageView = null;

	private static final int MSG_CONNECTION_ONLINE = 1;
	private static final int MSG_CONNECTION_OFFLINE = 2;
	private static final int MSG_CONNECTION_TIMEOUT = 3;
	private static final int JIBE_CONNECTION_STARTUP_TIMEOUT_MILLIS = 10000;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		mStatusView = (TextView) findViewById(R.id.statusView);

		mImageView = (ImageView) findViewById(R.id.swirlView);

		AnimationDrawable frameAnimationRemote = (AnimationDrawable) mImageView.getBackground();
		frameAnimationRemote.setCallback(mImageView);
		frameAnimationRemote.setOneShot(false);
		frameAnimationRemote.setVisible(true, true);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public void onStart() {
		super.onStart();

		mIsEngineInstalled = ArenaHelper.isCompatibleRealtimeEngineInstalled(this);

		if (mIsEngineInstalled) {
			if (mDownloadDialog != null) {
				destroyDownloadDialog();
			}

			if (mMyProfileHelper == null) {
				// connecting to the MyProfileHelper will establish a service
				// connection to the Jibe RTE
				// if the user has not already signed up/logged in this (or any
				// other feature requiring
				// a connection to the Jibe engine) will AUTOMATICALLY trigger
				// the jibe login flow.
				mMyProfileHelper = new MyProfileHelper(this, mMyProfileHelperListener, mOnlineStateListener);
			}
		} else {
			if (mDownloadDialog == null) {
				createDownloadDialog();
			}
		}

	}

	private void startConnectionStartupTimer() {
		mHandler.sendEmptyMessageAtTime(MSG_CONNECTION_TIMEOUT, SystemClock.uptimeMillis() + JIBE_CONNECTION_STARTUP_TIMEOUT_MILLIS);
	}

	private SimpleApiStateListener mMyProfileHelperListener = new SimpleApiStateListener() {

		public void onInitialized(SimpleApi source) {
			try {
				if (mMyProfileHelper.isOnline()) {
					handleOnlineStateChange(true);
				} else {
					mMyProfileHelper.startMonitoringOnlineState();
					startConnectionStartupTimer();
				}
			} catch (Exception e) {
				Log.e(TAG, "Failed to start MyProfileHelper", e);
			}
		}

		public void onInitializationFailed(SimpleApi source, ConnectFailedReason reason) {
			Log.e(TAG, "Initialization of MyProfileHelper has failed. Reason=" + reason);
		}
	};

	private OnlineStateListener mOnlineStateListener = new OnlineStateListener() {
		public void onOnlineStateChanged(boolean isOnline) {
			handleOnlineStateChange(isOnline);
		}
	};

	private void handleOnlineStateChange(boolean isOnline) {
		if (isOnline) {
			try {
				mMyProfileHelper.stopMonitoringOnlineState();
			} catch (Exception e) {
				e.printStackTrace();
			}
			mHandler.sendEmptyMessage(MSG_CONNECTION_ONLINE);
		} else {
			mHandler.sendEmptyMessage(MSG_CONNECTION_OFFLINE);
		}
	}

	public void onClickLaunchFriendPicker(View view) {
		mArenaHelper = new ArenaHelper(this, mArenaHelperListener);
	}

	public void onClickComputerPlayer(View view) {
		Intent mainIntent = new Intent(this, MainActivity.class);
		mainIntent.putExtra(RPSHelper.KEY_PLAYER_COMPUTER, true);
		startActivity(mainIntent);
	}

	private Boolean isOnLine = false;
	private Handler mHandler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}

			switch (msg.what) {
			case MSG_CONNECTION_ONLINE:
				isOnLine = true;
				Button btn = (Button) Bootstrap.this.findViewById(R.id.friendPickerButton);
				btn.setEnabled(true);
				mArenaHelper = new ArenaHelper(Bootstrap.this, mArenaHelperListener);
				mStatusView.setText(R.string.connection_online);
				break;
			case MSG_CONNECTION_OFFLINE:
				isOnLine = false;
				((Button) Bootstrap.this.findViewById(R.id.friendPickerButton)).setEnabled(false);
				mStatusView.setText(R.string.connection_offline);
				break;
			case MSG_CONNECTION_TIMEOUT:
				isOnLine = false;
				try {
					if (!mMyProfileHelper.isOnline()) {
						((Button) Bootstrap.this.findViewById(R.id.friendPickerButton)).setEnabled(false);
						mStatusView.setText(R.string.connection_failed);
					}
				} catch (Exception ex) {
				}
				break;
			}
		}
	};

	public void onDestroy() {
		super.onDestroy();

		if (mMyProfileHelper != null) {
			mMyProfileHelper.dispose();
		}
		if (mArenaHelper != null) {
			mArenaHelper.dispose();
		}
	}

	private void createDownloadDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dlg_download_title);
		builder.setMessage(R.string.dlg_download_message);
		builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				ArenaHelper.openGooglePlayPage(Bootstrap.this);
			}
		});

		builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Bootstrap.this.finish();
			}
		});

		builder.setCancelable(false);

		mDownloadDialog = builder.create();
		mDownloadDialog.show();
	}

	private void destroyDownloadDialog() {
		if (mDownloadDialog.isShowing()) {
			mDownloadDialog.dismiss();
		}
		mDownloadDialog = null;
	}

	private SimpleApiStateListener mArenaHelperListener = new SimpleApiStateListener() {
		public void onInitialized(SimpleApi source) {
			try {
				mArenaHelper.startFriendPicker();
			} catch (JibeServiceException e) {
				Log.e(TAG, "Failed to startFriendPicker()", e);
			}
			mArenaHelper.dispose();
			mArenaHelper = null;
			finish();
		}

		public void onInitializationFailed(SimpleApi source, ConnectFailedReason reason) {
			Log.e(TAG, "Failed to initialize ArenaHelper. Reason=" + reason);
		}
	};

}
