package uk.co.madmouse.jibe.rps.utils;

public class RPSHelper {

	public static final String KEY_PLAYER_COMPUTER = "uk.co.madmouse.KEY_PLAYER_COMPUTER";
	public static final String KEY_TEXT = "uk.co.madmouse.KEY_TEXT";
	public static final String KEY_TIMESTAMP = "uk.co.madmouse.KEY_TIMESTAMP";
	public static final String KEY_WEAPON = "uk.co.madmouse.KEY_WEAPON";

	public enum WeaponSelection {
		none, rock, paper, scissors;

	}

	public enum SkirmishResult {
		tie, local, remote;
	}

	public static SkirmishResult determineRoundWinner(WeaponSelection localWeapon, WeaponSelection remoteWeapon) {

		// Check for ties.
		if (localWeapon.equals(remoteWeapon)) {
			return SkirmishResult.tie;
		}

		// if it is not a tie check for all the ways the
		// computer can win.
		// Rock smashes scissors...
		else if (localWeapon == WeaponSelection.rock && remoteWeapon == WeaponSelection.scissors) {
			return SkirmishResult.local;
		}
		// Paper covers rock...
		else if (localWeapon == WeaponSelection.paper && remoteWeapon == WeaponSelection.rock) {
			return SkirmishResult.local;
		}
		// Scissors cut paper...
		else if (localWeapon == WeaponSelection.scissors && remoteWeapon == WeaponSelection.paper) {
			return SkirmishResult.local;
		}
		// Its not a tie and the computer did not
		// win so the player 2 must have won!
		else {
			return SkirmishResult.remote;
		}
	}

	/**
	 * Pick the computer's move at random.
	 * 
	 * @return The computer's move as one of: Rock, Paper, Scissors
	 */
	public static WeaponSelection getComputerSelection() {
		int compMoveInt;

		// Generate a random move for the
		// computer.
		compMoveInt = randomInt(1, 3);

		// Convert the random integer into a
		// string that represents the computer's
		// move.
		if (compMoveInt == 1) {
			return WeaponSelection.rock;
		} else if (compMoveInt == 2) {
			return WeaponSelection.paper;
		} else {
			return WeaponSelection.scissors;
		}

	}

	/**
	 * Generate a random integer in the range [lowEnd...highEnd].
	 * 
	 * @param lowEnd
	 *            the low end of the range of possible numbers.
	 * @param highEnd
	 *            the high end of the range of possible numbers.
	 * @return a random integer in [lowEnd...highEnd]
	 */
	private static int randomInt(int lowEnd, int highEnd) {
		int theNum;

		// Pick a random double in the range [0...highEnd-lowEnd+1)
		// then truncate it to an integer and shift it up by lowEnd.
		theNum = (int) (Math.random() * (highEnd - lowEnd + 1)) + lowEnd;

		return theNum;
	}

}
