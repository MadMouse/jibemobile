package uk.co.madmouse.jibe.rps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jibe.sdk.client.JibeIntents;
import jibe.sdk.client.JibeServiceListener.ConnectFailedReason;
import jibe.sdk.client.events.JibeSessionEvent;
import jibe.sdk.client.simple.ChallengeDialog;
import jibe.sdk.client.simple.SimpleApi;
import jibe.sdk.client.simple.SimpleApiStateListener;
import jibe.sdk.client.simple.SimpleConnectionStateListener;
import jibe.sdk.client.simple.session.JibeBundle;
import jibe.sdk.client.simple.session.JibeBundleTransferConnection;
import jibe.sdk.client.simple.session.JibeBundleTransferConnection.JibeBundleTransferConnectionListener;
import jibe.sdk.client.video.CameraMediaSource;
import uk.co.madmouse.jibe.rps.utils.RPSHelper;
import uk.co.madmouse.jibe.rps.utils.RPSHelper.SkirmishResult;
import uk.co.madmouse.jibe.rps.utils.RPSHelper.WeaponSelection;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();

	private Boolean mIsComputerPlayer = false;
	public boolean mIsSender;
	private String mChallengeReceiverDisplayName = null;
	private String mChallengerDisplayName = null;

	// private MyProfileHelper mMyProfileHelper;

	private ProgressDialog mWaitingForReceiverToAcceptDialog = null;
	private ChallengeDialog mIncomingChallengeDialog = null;

	private JibeBundleTransferConnection mBundleConnection = null;

	//	private VideoCallConnection mVideoConnection = null;

	private SurfaceView mLocalViewSurface;
	private SurfaceView mRemoteViewSurface;
	private CameraMediaSource mCameraMediaSource;

	public boolean mIsNetworkFailure = false;
	public boolean mGameStart = false;

	private ImageView mRemotePlayerWeapon;
	private TextView mRemotePlayerWeaponText;
	private WeaponSelection mRemoteWeapon = WeaponSelection.none;

	private ImageView mLocalPlayerWeapon;
	private TextView mLocalPlayerWeaponText;
	private WeaponSelection mLocalWeapon = WeaponSelection.none;
	private ImageView mLocaleWin_1 = null;
	private ImageView mLocaleWin_2 = null;
	private ImageView mLocaleWin_3 = null;

	private int mWonCount = 0;
	private int mLostCount = 0;
	private int mTieCount = 0;


	List<SkirmishResult> mSkirmish = new ArrayList<SkirmishResult>();


	private TextView mPosistionView;

	private ImageButton mBtnRock = null;
	private ImageButton mBtnPaper = null;
	private ImageButton mBtnScissors = null;
	private Button mBtnPlayAgain = null;

	private void handleRemoteWeapon(WeaponSelection weapon) {
		if (mRemotePlayerWeapon != null) {
			if (RPSHelper.WeaponSelection.paper.equals(weapon)) {
				mRemotePlayerWeapon.setBackgroundResource(R.drawable.paper_selected);
			} else if (RPSHelper.WeaponSelection.rock.equals(weapon)) {
				mRemotePlayerWeapon.setBackgroundResource(R.drawable.rock_selected);
			}
			if (RPSHelper.WeaponSelection.scissors.equals(weapon)) {
				mRemotePlayerWeapon.setBackgroundResource(R.drawable.scissors_selected);
			}

			if (mRemotePlayerWeaponText != null) {
				mRemotePlayerWeaponText.setText(weapon.name());
			}

			mRemotePlayerWeapon.setVisibility(View.VISIBLE);
			AnimationDrawable frameAnimationRemote = (AnimationDrawable) mRemotePlayerWeapon.getBackground();
			frameAnimationRemote.setCallback(mRemotePlayerWeapon);
			frameAnimationRemote.setOneShot(true);
			frameAnimationRemote.setVisible(true, true);
		}
	}

	private void handleLocalWeapon(WeaponSelection weapon) {
		if (mLocalPlayerWeapon != null) {

			if (RPSHelper.WeaponSelection.paper.equals(weapon)) {
				mLocalPlayerWeapon.setBackgroundResource(R.drawable.paper_selected);
			} else if (RPSHelper.WeaponSelection.rock.equals(weapon)) {
				mLocalPlayerWeapon.setBackgroundResource(R.drawable.rock_selected);
			}
			if (RPSHelper.WeaponSelection.scissors.equals(weapon)) {
				mLocalPlayerWeapon.setBackgroundResource(R.drawable.scissors_selected);
			}

			if (mLocalPlayerWeaponText != null) {
				mLocalPlayerWeaponText.setText(weapon.name());
			}

			mLocalPlayerWeapon.setVisibility(View.VISIBLE);
			AnimationDrawable frameAnimationLocal = (AnimationDrawable) mLocalPlayerWeapon.getBackground();
			if (frameAnimationLocal.isRunning()) {
				frameAnimationLocal.stop();
			}
			frameAnimationLocal.setCallback(mLocalPlayerWeapon);
			frameAnimationLocal.setOneShot(true);
			frameAnimationLocal.setVisible(true, true);

		}
	}

	private SkirmishResult weaponPlayed() {
		if (mRemoteWeapon != WeaponSelection.none && mLocalWeapon != WeaponSelection.none) {

			if (mPosistionView != null) {
				mPosistionView.setText(null);
			}

			handleRemoteWeapon(mRemoteWeapon);
			handleLocalWeapon(mLocalWeapon);

			if (mRemoteWeapon != WeaponSelection.none && mLocalWeapon != WeaponSelection.none) {
				SkirmishResult result = RPSHelper.determineRoundWinner(mLocalWeapon, mRemoteWeapon);
				if (mPosistionView != null) {
					if (result.equals(RPSHelper.SkirmishResult.local)) {
						mPosistionView.setText(getString(R.string.pos_winner));
						mWonCount++;
						mSkirmish.add(result);
					} else if (result.equals(RPSHelper.SkirmishResult.remote)) {
						mPosistionView.setText(getString(R.string.pos_loser));
						mLostCount++;
						mSkirmish.add(result);
					} else {
						mPosistionView.setText(getString(R.string.pos_tie));
						mTieCount++;
						mSkirmish.add(result);
					}
				}

				if(mLocaleWin_3 != null){
					mLocaleWin_3.setVisibility(View.INVISIBLE);
				}
				if(mLocaleWin_2 != null){
					mLocaleWin_2.setVisibility(View.INVISIBLE);
				}
				if(mLocaleWin_1 != null){
					mLocaleWin_1.setVisibility(View.INVISIBLE);
				}


				for(int i= 0; i < mSkirmish.size(); i++){
					switch(i){
					case 0 :
						if(mLocaleWin_1 != null){
							mLocaleWin_1.setImageResource(getImageResource(mSkirmish.get(i)));
							mLocaleWin_1.setVisibility(View.VISIBLE);

						}
						break;
					case 1 :
						if(mLocaleWin_2 != null){
							mLocaleWin_2.setImageResource(getImageResource(mSkirmish.get(i)));
							mLocaleWin_2.setVisibility(View.VISIBLE);

						}
						break;
					case 2 :
						if(mLocaleWin_3 != null){
							mLocaleWin_3.setImageResource(getImageResource(mSkirmish.get(i)));
							mLocaleWin_3.setVisibility(View.VISIBLE);

							if ((mTieCount == 1 && mLostCount == 1 && mWonCount == 1) || 
									(mTieCount > 1)){
								mPosistionView.setText(getString(R.string.result_tie));
							} else if(mWonCount > 1){
								mPosistionView.setText(getString(R.string.result_won));
							} else {
								mPosistionView.setText(getString(R.string.result_lost));
							}
							if(mBtnPlayAgain != null){
								mBtnPlayAgain.setVisibility(View.VISIBLE);
							}
							if(mBtnRock != null){
								mBtnRock.setVisibility(View.GONE);
							}
							if(mBtnScissors != null){
								mBtnScissors.setVisibility(View.GONE);
							}
							if(mBtnPaper != null){
								mBtnPaper.setVisibility(View.GONE);
							}
						}
					}

				}
				mLocalWeapon = WeaponSelection.none;
				mRemoteWeapon = WeaponSelection.none;
				return result;
			}
		}
		return null;
	}

	private Integer getImageResource(SkirmishResult result){
		if(result == SkirmishResult.tie){
			return R.drawable.tie;
		} else if(result == SkirmishResult.local){
			return R.drawable.win;
		} 
		return R.drawable.lost;
	}

	private SkirmishResult localWeaponPlayed(WeaponSelection localWeapon) {
		this.mLocalWeapon = localWeapon;
		if (mPosistionView != null) {
			mPosistionView.setText(getString(R.string.pos_choosen, localWeapon.name()));
		}
		if (mRemoteWeapon == WeaponSelection.none) {
			if (mLocalPlayerWeapon != null) {
				mLocalPlayerWeapon.setVisibility(View.INVISIBLE);
			}
			if (mLocalPlayerWeaponText != null) {
				mLocalPlayerWeaponText.setText(null);
			}

			if (mRemotePlayerWeapon != null) {
				mRemotePlayerWeapon.setVisibility(View.INVISIBLE);
			}

			if (mRemotePlayerWeaponText != null) {
				mRemotePlayerWeaponText.setText(null);
			}
		}

		if (mIsComputerPlayer) {
			return remoteWeaponPlayed(RPSHelper.getComputerSelection());
		} else {
			sendData(localWeapon);
			return weaponPlayed();
		}
	}

	private void sendData(WeaponSelection localWeapon) {
		JibeBundle bundle = new JibeBundle();

		long timestamp = System.currentTimeMillis();

		bundle.putLong(RPSHelper.KEY_TIMESTAMP, timestamp);
		bundle.putInt(RPSHelper.KEY_WEAPON, localWeapon.ordinal());

		Log.d(TAG, "Sending " + localWeapon + ", " + timestamp);

		try {
			int packetNumber = mBundleConnection.send(bundle);
			Log.d(TAG, "Sent packet number: " + packetNumber);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private SkirmishResult remoteWeaponPlayed(WeaponSelection remoteWeapon) {
		this.mRemoteWeapon = remoteWeapon;

		if (mLocalWeapon == WeaponSelection.none) {
			
			playAgain();
			if (mLocalWeapon == WeaponSelection.none) {
				if (mPosistionView != null) {
					mPosistionView.setText(getString(R.string.pos_choose));
				}

				if (mLocalPlayerWeapon != null) {
					mLocalPlayerWeapon.setVisibility(View.INVISIBLE);
				}
				if (mLocalPlayerWeaponText != null) {
					mLocalPlayerWeaponText.setText(null);
				}

				if (mRemotePlayerWeapon != null) {
					mRemotePlayerWeapon.setVisibility(View.INVISIBLE);

				}
				if (mRemotePlayerWeaponText != null) {
					mRemotePlayerWeaponText.setText(null);
				}
			}
			resetWeapons();
		}
		return weaponPlayed();
	}

	private void resetWeapons() {

		if (mBtnPaper != null) {
			mBtnPaper.setSelected(false);
			mBtnPaper.setVisibility(View.VISIBLE);
		}

		if (mBtnRock != null) {
			mBtnRock.setSelected(false);
			mBtnRock.setVisibility(View.VISIBLE);
		}

		if (mBtnScissors != null) {
			mBtnScissors.setSelected(false);
			mBtnScissors.setVisibility(View.VISIBLE);
		}
	}

	private void playAgain(){
		
		if(mBtnPlayAgain != null && mBtnPlayAgain.getVisibility() == View.VISIBLE){
			mBtnPlayAgain.setVisibility(View.GONE);
			
			if(mLocaleWin_3 != null){
				mLocaleWin_3.setVisibility(View.INVISIBLE);
			}
			if(mLocaleWin_2 != null){
				mLocaleWin_2.setVisibility(View.INVISIBLE);
			}
			if(mLocaleWin_1 != null){
				mLocaleWin_1.setVisibility(View.INVISIBLE);
			}
			mSkirmish.clear();
		}
		
		
	}
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mPosistionView = (TextView) findViewById(R.id.positionView);

		mBtnPlayAgain = (Button) findViewById(R.id.btn_PlayAgain);
		if(mBtnPlayAgain != null){
			mBtnPlayAgain.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					playAgain();

					
					resetWeapons();
				}
			});
		}

		mBtnRock = (ImageButton) findViewById(R.id.btn_Rock);
		if (mBtnRock != null) {
			mBtnRock.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					localWeaponPlayed(RPSHelper.WeaponSelection.rock);
					resetWeapons();
					mBtnRock.setSelected(true);

				}
			});
		}

		mBtnPaper = (ImageButton) findViewById(R.id.btn_Paper);
		if (mBtnPaper != null) {
			mBtnPaper.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					localWeaponPlayed(RPSHelper.WeaponSelection.paper);
					resetWeapons();
					mBtnPaper.setSelected(true);
				}
			});
		}

		mBtnScissors = (ImageButton) findViewById(R.id.btn_Scissors);
		if (mBtnScissors != null) {
			mBtnScissors.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					localWeaponPlayed(RPSHelper.WeaponSelection.scissors);
					resetWeapons();
					mBtnScissors.setSelected(true);
				}
			});
		}

		mBundleConnection = new JibeBundleTransferConnection(this, mBundleConnStateListener, mBundleListener);

		Intent i = getIntent();

		this.mIsComputerPlayer = i.getBooleanExtra(RPSHelper.KEY_PLAYER_COMPUTER, false);
		// Check if this activity was launched via the Arena friend list. If
		// yes, you are the sender/creator of the session
		mIsSender = i.getAction().startsWith(JibeIntents.ACTION_ARENA_CHALLENGE);

		// Init surfaces and camera
		mLocalViewSurface = (SurfaceView) findViewById(R.id.video_local);
		mLocalPlayerWeapon = (ImageView) findViewById(R.id.localUserWeapon);

		View localWinView = findViewById(R.id.localWinnerArray);
		mLocaleWin_1 = (ImageView) localWinView.findViewById(R.id.win_1);
		mLocaleWin_2 = (ImageView) localWinView.findViewById(R.id.win_2);
		mLocaleWin_3 = (ImageView) localWinView.findViewById(R.id.win_3);


		mCameraMediaSource = new CameraMediaSource(CameraMediaSource.CAMERA_FRONT, mLocalViewSurface);

		mRemoteViewSurface = (SurfaceView) findViewById(R.id.video_remote);
		mRemotePlayerWeapon = (ImageView) findViewById(R.id.remoteUserWeapon);

		if (mIsSender) {
			mChallengeReceiverDisplayName = i.getStringExtra(JibeIntents.EXTRA_DISPLAYNAME);
			//			mChallengeReceiverPhoneNumber = i.getStringExtra(JibeIntents.EXTRA_USERID);

			showSenderSideDialog();
		} else {
			mChallengerDisplayName = i.getStringExtra(JibeIntents.EXTRA_DISPLAYNAME);
			showReceiverSideDialog();
		}

	}

	private void showReceiverSideDialog() {
		mIncomingChallengeDialog = new ChallengeDialog(this);
		mIncomingChallengeDialog.setPlayRingtone(true);
		mIncomingChallengeDialog.setVibrate(true);
		mIncomingChallengeDialog.setMessage(getString(R.string.dlg_incoming_challenge) + " " + mChallengerDisplayName);
		mIncomingChallengeDialog.show(mChallengeDialogListener, getIntent());
	}

	private void showSenderSideDialog() {
		mWaitingForReceiverToAcceptDialog = new ProgressDialog(MainActivity.this);
		if (mChallengeReceiverDisplayName != null) {
			mWaitingForReceiverToAcceptDialog.setMessage(getString(R.string.outgoing_challenge_dialog_message, mChallengeReceiverDisplayName));
		} else {
			mWaitingForReceiverToAcceptDialog.setMessage(getString(R.string.outgoing_challenge_dialog_search_message));
		}
		mWaitingForReceiverToAcceptDialog.setIndeterminate(true);
		mWaitingForReceiverToAcceptDialog.setCancelable(true);
		mWaitingForReceiverToAcceptDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}

		});
		mWaitingForReceiverToAcceptDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				finish();
			}
		});
		mWaitingForReceiverToAcceptDialog.show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mBundleConnection != null) {
			mBundleConnection.dispose();
		}
		//VIDEO		if (mVideoConnection != null) {
		//			mVideoConnection.dispose();
		//		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private ChallengeDialog.Listener mChallengeDialogListener = new ChallengeDialog.Listener() {
		@Override
		public void onAccepted(ChallengeDialog source, Intent intent) {
			try {
				mBundleConnection.start(getIntent());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onRejected(ChallengeDialog source) {
			finish();
		}

		@Override
		public void onError(ChallengeDialog source, int reason) {
			finish();
		}
	};

	private JibeBundleTransferConnectionListener mBundleListener = new JibeBundleTransferConnectionListener() {

		@Override
		public void jibeBundleReceived(JibeBundle jibeBundle) {
			Log.d(TAG, "Received bundle=" + jibeBundle);
			long timestamp = jibeBundle.getLong("ts", -1);
			String str = "Received TS: " + timestamp;

			int ordinalValue = jibeBundle.getInt(RPSHelper.KEY_WEAPON, 0);
			Message remoteMSG = new Message();
			remoteMSG.arg1 = ordinalValue;
			mRemoteHandler.sendMessage(remoteMSG);
		}

		@Override
		public void acknowledgeReceived(int bundleNumber) {
			Log.d(TAG, "Received ack for bundle number: " + bundleNumber);
		}
	};

	private Handler mRemoteHandler = new Handler() {
		@Override
		public void handleMessage(final Message msg) {
			if (isFinishing()) {
				return;
			}
			remoteWeaponPlayed(WeaponSelection.values()[msg.arg1]);
		}
	};

	private void showMessage(final String message) {
		if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					showMessage(message);
				}
			});

			return;
		}

		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	private SimpleConnectionStateListener mBundleConnStateListener = new SimpleConnectionStateListener() {

		@Override
		public void onInitialized(SimpleApi source) {
			Log.d(TAG, "onInitialized()");
			try {
				if (mIsSender) {
					mBundleConnection.start(getIntent());
				} else {
					mBundleConnection.monitor(getIntent());
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception opening datagram", e);
			}
		}

		@Override
		public void onInitializationFailed(SimpleApi source, ConnectFailedReason reason) {
			Log.e(TAG, "onInitializationFailed()");
		}

		@Override
		public void onStarted(SimpleApi source) {
			Log.v(TAG, "onStarted()");
			doWhenDataConnectionStarted();
			// if (mMyProfileHelper == null) {
			// mMyProfileHelper = new MyProfileHelper(MainActivity.this,
			// mMyProfileHelperListener);
			// }

			if (mCameraMediaSource != null && mRemoteViewSurface != null) {
				//Video				mVideoConnection = new VideoCallConnection(getApplicationContext(), mVideoConnStateListener, mCameraMediaSource, mRemoteViewSurface);
				//				if (mVideoConnection != null) {
				//					mVideoConnection.setAutoAccept(true);
				//				}
			}

		}

		private SimpleConnectionStateListener mVideoConnStateListener = new SimpleConnectionStateListener() {

			@Override
			public void onInitialized(SimpleApi source) {
				Log.v(TAG, "onInitialized()");
				if (mIsSender) {
					try {
						//VIDEO						mVideoConnection.start(mChallengeReceiverPhoneNumber);
					} catch (Exception e) {
						Log.e(TAG, "mTwoWayVideoConnection. Failed to open()", e);
						showMessage("Failed to open video connection");
					}
				}
			}

			@Override
			public void onInitializationFailed(SimpleApi source, ConnectFailedReason reason) {
			}

			@Override
			public void onStarted(SimpleApi source) {
				Log.v(TAG, "onStarted()");
				showMessage("Connection started");
			}

			@Override
			public void onStartFailed(SimpleApi source, int info) {
				Log.e(TAG, "onStartFailed(). JibeSessionEvent info:" + info);
			}

			@Override
			public void onTerminated(SimpleApi source, int info) {
				Log.v(TAG, "onTerminated(). JibeSessionEvent info:" + info);
			}
		};

		private SimpleApiStateListener mMyProfileHelperListener = new SimpleApiStateListener() {

			public void onInitialized(SimpleApi source) {
				try {
					// if (mMyProfileHelper.isOnline()) {
					// } else {
					// mMyProfileHelper.startMonitoringOnlineState();
					// }
				} catch (Exception e) {
					Log.e(TAG, "Failed to start MyProfileHelper", e);
				}
			}

			public void onInitializationFailed(SimpleApi source, ConnectFailedReason reason) {
				Log.e(TAG, "Initialization of MyProfileHelper has failed. Reason=" + reason);
			}
		};

		@Override
		public void onStartFailed(SimpleApi source, int info) {
			Log.v(TAG, "onStartFailed(). JibeSessionEvent info:" + info);
			switch (info) {
			case JibeSessionEvent.INFO_SESSION_CANCELLED:
				showMessage("The sender has canceled the connection");
				break;
			case JibeSessionEvent.INFO_SESSION_REJECTED:
				showMessage("The receiver has rejected the connection/is busy");
				break;
			case JibeSessionEvent.INFO_USER_NOT_ONLINE:
				showMessage("Receiver is not online");
				break;
			case JibeSessionEvent.INFO_USER_UNKNOWN:
				showMessage("This phone number is not known");
				break;
			default:
				showMessage("Connection start failed.");
				break;
			}
		}

		@Override
		public void onTerminated(SimpleApi source, int info) {
			Log.v(TAG, "onTerminated(). JibeSessionEvent info:" + info);
			switch (info) {
			case JibeSessionEvent.INFO_GENERIC_EVENT:
				showMessage("You terminated the connection");
				break;
			case JibeSessionEvent.INFO_SESSION_TERMINATED_BY_REMOTE:
				showMessage("The remote party terminated the connection");
				break;
			default:
				showMessage("Connection terminated.");
				break;
			}
			finish();
		}
	};

	private void doWhenDataConnectionStarted() {
		if (mIsSender) {
			mWaitingForReceiverToAcceptDialog.dismiss();
		} else {
			mIncomingChallengeDialog.dismiss();
		}
	}

}
