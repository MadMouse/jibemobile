package uk.co.madmouse.jibe.rps.widgets;

import jibe.sdk.client.apptoapp.AppToAppDatabaseConstants;
import jibe.sdk.client.apptoapp.AppToAppDatabaseConstants.Columns;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UserRelativeLayout extends RelativeLayout {

	Long mId = -1l;
	Long mJibeUserId = -1l;
	Long mRawContactId = -1l;
	Long mContactId = -1l;
	SurfaceView mUserSurfaceView = null;
	ImageView mUserImage = null;
	TextView mUserDisplayName = null;
	TextView mNickName = null;

	String[] friendsProjection = { Columns.JibeUsers._ID, Columns.JibeUsers.NICKNAME, Columns.JibeUsers.MSISDN, Columns.JibeUsers.JIBE_USERID, Columns.JibeUsers.CONTACT_ID,
			Columns.JibeUsers.RAW_CONTACT_ID };

	public void setAppId(String appID, String displayName, String MSISDN) {

		String[] appsProjection = { Columns.JibeUserApps.JIBE_USERID };
		String appsSelection = Columns.JibeUserApps.APP_ID + "=?";
		String[] appsSelectionArgs = { appID };

		String friendsSelection = null;
		// cursor containing the jibe user ids of all contacts who have this app
		Cursor cursor = getContext().getContentResolver().query(AppToAppDatabaseConstants.JIBE_USER_APPS_CONTENT_URI, appsProjection, appsSelection, appsSelectionArgs, null);
		try {
			// create a string which can be used in an SQL IN clause
			String friendIdSet = createSqlQuerySet(cursor, cursor.getColumnIndex(Columns.JibeUserApps.JIBE_USERID));
			friendsSelection = Columns.JibeUsers.JIBE_USERID + " IN " + friendIdSet + " and " + Columns.JibeUsers.MSISDN + " = '" + MSISDN + "'";
		} finally {
			cursor.close();
		}

		// cursor containing all nick names and msidns of users who have this
		// app
		cursor = getContext().getContentResolver().query(AppToAppDatabaseConstants.JIBE_USERS_CONTENT_URI, friendsProjection, friendsSelection, null, Columns.JibeUsers.NICKNAME + " ASC");

		if (cursor != null) {
			try {
				applyCursor(cursor, displayName);
			} finally {
				cursor.close();
			}
		}

	}

	public void setUserId(String jibeUserID, String displayName) {
		String friendsSelection = Columns.JibeUsers.JIBE_USERID + " = " + jibeUserID;
		// cursor containing all nick names and msidns of users who have this
		// app
		Cursor cursor = getContext().getContentResolver().query(AppToAppDatabaseConstants.JIBE_USERS_CONTENT_URI, friendsProjection, friendsSelection, null, Columns.JibeUsers.NICKNAME + " ASC");

		if (cursor != null) {
			try {
				applyCursor(cursor, displayName);
			} finally {
				cursor.close();
			}
		}

	}

	private void applyCursor(Cursor cursor, String displayName) {
		if (cursor.moveToFirst()) {
			this.mId = cursor.getLong(cursor.getColumnIndexOrThrow(Columns.JibeUsers._ID));
			this.mJibeUserId = cursor.getLong(cursor.getColumnIndexOrThrow(Columns.JibeUsers.JIBE_USERID));
			this.mRawContactId = cursor.getLong(cursor.getColumnIndexOrThrow(Columns.JibeUsers.RAW_CONTACT_ID));
			this.mContactId = cursor.getLong(cursor.getColumnIndexOrThrow(Columns.JibeUsers.CONTACT_ID));

			if (mUserSurfaceView != null) {

			}

			if (mUserImage != null) {

			}

			if (mUserDisplayName != null) {
				mUserDisplayName.setText(displayName);
			}

			if (mNickName != null) {
				mNickName.setText(cursor.getString(cursor.getColumnIndexOrThrow(Columns.JibeUsers.NICKNAME)));
			}
		}

	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		// mUserSurfaceView = (SurfaceView) findViewById(R.id.userSurface);
		// mUserImage = (ImageView) findViewById(R.id.userImage);
		// mUserDisplayName = (TextView) findViewById(R.id.userDisplayName);
		// mNickName = (TextView) findViewById(R.id.userNickName);

	}

	public UserRelativeLayout(Context context) {
		super(context);
	}

	public UserRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UserRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * Creates a list of elements that can be used in an SQL statement, i.e.
	 * "(a, b, c, d, ...)".
	 * 
	 * @param cursor
	 *            A cursor containing the elements to be added to the set
	 * @param columnIndex
	 *            Column index of the elements to be added.
	 * @return A string formatted in a form that can be used in an SQL statement
	 */
	private String createSqlQuerySet(Cursor cursor, int columnIndex) {
		if ((cursor == null) || (cursor.isClosed()) || (cursor.getCount() == 0)) {
			return "()";
		}
		StringBuilder builder = new StringBuilder();
		builder.append('(');

		cursor.moveToFirst();
		String value = cursor.getString(columnIndex);
		builder.append(value);

		while (cursor.moveToNext()) {
			value = cursor.getString(columnIndex);
			if (!TextUtils.isEmpty(value)) {
				builder.append(", ");
				builder.append(value);
			}
		}

		builder.append(')');
		return builder.toString();
	}

}
